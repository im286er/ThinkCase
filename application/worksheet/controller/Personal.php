<?php

// +----------------------------------------------------------------------
// | ThinkAdmin
// +----------------------------------------------------------------------
// | 版权所有 2014~2019 广州楚才信息科技有限公司 [ http://www.cuci.cc ]
// +----------------------------------------------------------------------
// | 官方网站: http://demo.thinkadmin.top
// +----------------------------------------------------------------------
// | 开源协议 ( https://mit-license.org )
// +----------------------------------------------------------------------
// | gitee 代码仓库：https://gitee.com/zoujingli/ThinkAdmin
// | github 代码仓库：https://github.com/zoujingli/ThinkAdmin
// +----------------------------------------------------------------------

namespace app\worksheet\controller;

use library\Controller;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\IOFactory;

/**
 * 个人工单管理
 * Class Auth
 * @package app\worksheet\controller
 */
class Personal extends Controller
{
    /**
     * 指定数据表
     * @var string
     */
    protected $table = 'WorkSheet';

    /**
     * 个人工单列表
     * @auth true  # 表示需要验证权限
     * @menu true  # 在菜单编辑的节点可选项
     */
    public function index()
    {
        $this->title = '个人工单管理';
        $query = $this->_query($this->table)->where("uid", session('admin_user.id'))->where("work_status", ">=", 0);

        if (!empty($this->request->get("customer"))) {
            $query = $query->where("customer", $this->request->get("customer"));
        }
        if (!empty($this->request->get("status"))) {
            $query = $query->where("work_status", $this->request->get("status"));
        }

        $query = $query->order('work_id desc')->page();
    }

    /**
     * 新增个人工单
     * @auth true  # 表示需要验证权限
     * @menu true  # 在菜单编辑的节点可选项
     */
    public function add()
    {
        $this->title = '创建工单';
        $this->applyCsrfToken();
        $this->_form($this->table, 'form');
    }

    /**
     * 编辑个人工单
     * @auth true  # 表示需要验证权限
     * @menu true  # 在菜单编辑的节点可选项
     */
    public function edit()
    {
        $this->title = '修改工单';
        $this->applyCsrfToken();
        $this->_form($this->table, 'form');
    }

    /**
     * 个人工单状态更改
     * @auth true  # 表示需要验证权限
     * @menu true  # 在菜单编辑的节点可选项
     */
    public function state()
    {
        $this->_save($this->table, ['work_status' => input('work_status', '0')]);
    }

    /**
     * 删除个人工单
     * @auth true  # 表示需要验证权限
     * @menu true  # 在菜单编辑的节点可选项
     */
    public function remove()
    {
        $this->_delete($this->table);
    }

    /**
     * 查看个人工单
     * @auth true  # 表示需要验证权限
     * @menu true  # 在菜单编辑的节点可选项
     */
    public function info()
    {
        $this->title = '工单信息表';
        $this->_form($this->table, 'info');
    }

    protected function _form_filter(&$data)
    {
        if (isset($data["support_name"])) {
            $data["support"] = 1;
        }
        if (isset($data["contact_people"])) {
            $data["customer_phone"] = config("contact_people")[$data["contact_people"]];
        }
        $data["uid"] = session("admin_user.id");
    }

    /**
     * 批量导出个人工单
     * @auth true  # 表示需要验证权限
     * @menu true  # 在菜单编辑的节点可选项
     */
    public function export()
    {
        $query = $this->_query($this->table)
            ->alias("a")
            ->join("system_user b","a.uid=b.id")
            ->where("a.uid", session('admin_user.id'))
            ->where("a.work_status", ">=", 0);

        if (!empty($this->request->get("customer"))) {
            $query = $query->where("a.customer", $this->request->get("customer"));
        }
        if (!empty($this->request->get("status"))) {
            $query = $query->where("a.work_status", $this->request->get("status"));
        }

        $query = $query->page($this->request->get("page"), $this->request->get("limit"));

        $spreadsheet = new Spreadsheet();
        $worksheet = $spreadsheet->getActiveSheet();
        //设置工作表标题名称
        $worksheet->setTitle('服务记录');
        //表头
        $worksheet->getColumnDimension("A")->setWidth(5);
        $worksheet->getColumnDimension("B")->setWidth(10);
        $worksheet->getColumnDimension("C")->setWidth(13);
        $worksheet->getColumnDimension("D")->setWidth(13);
        $worksheet->getColumnDimension("E")->setWidth(30);
        $worksheet->getColumnDimension("F")->setWidth(13);
        $worksheet->getColumnDimension("G")->setWidth(16);
        $worksheet->getColumnDimension("H")->setWidth(50);
        $worksheet->getColumnDimension("I")->setWidth(20);
        $worksheet->getColumnDimension("J")->setWidth(20);
        $worksheet->getColumnDimension("K")->setWidth(10);
        $worksheet->getColumnDimension("L")->setWidth(30);
        $worksheet->getStyle('H1')->getAlignment()->setWrapText(true);
        //设置单元格内容
        $worksheet->setCellValue("A1", '序号');
        $worksheet->setCellValue("B1", '工程师');
        $worksheet->setCellValue("C1", '客户名称');
        $worksheet->setCellValue("D1", '客户联系人');
        $worksheet->setCellValue("E1", '服务标题');
        $worksheet->setCellValue("F1", '服务分类');
        $worksheet->setCellValue("G1", '服务等级');
        $worksheet->setCellValue("H1", '服务内容');
        $worksheet->setCellValue("I1", '开始时间');
        $worksheet->setCellValue("J1", '结束时间');
        $worksheet->setCellValue("K1", '服务状态');
        $worksheet->setCellValue("L1", '备注');

        function html2txt($html){
            $html=str_replace("<p>","",$html);
            $html=str_replace("</p>","\r\n",$html);
            return $html;
        }
        foreach ($query as $k=>$v){
            foreach ($v as $i=>$item){
                $j=$i+2;
                $worksheet->setCellValue("A$j", $j);
                $worksheet->setCellValue("B$j", $item["nickname"]);
                $worksheet->setCellValue("C$j", config("customer")[$item["customer"]]);
                $worksheet->setCellValue("D$j", $item["contact_people"]);
                $worksheet->setCellValue("E$j", $item["work_desc"]);
                $worksheet->setCellValue("F$j", config("case_category")[$item["categroy"]]);
                $worksheet->setCellValue("G$j", config("case_grade")[$item["grade"]]);
                $worksheet->setCellValue("H$j", html2txt($item['work_content']));
                $worksheet->setCellValue("I$j", $item['work_start']);
                $worksheet->setCellValue("J$j", $item['work_end']);
                $worksheet->setCellValue("K$j", config("work_status")[$item['work_status']]);
                $worksheet->setCellValue("L$j", $item['ctime']);
            }
        }

        $worksheet->getStyle('A1:L1')->getFont()->setBold(true)->setName('微软雅黑 Light');

        $writer = new Xlsx($spreadsheet);
        $filename = 'service record.xlsx';
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="' . $filename . '"');
        header('Cache-Control: max-age=0');
        $writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
        $writer->save('php://output');
    }

}
